package ping

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestPingHandler(t *testing.T) {
	assert := assert.New(t)

	mux := mux.NewRouter()
	mux.HandleFunc("/ping", Ping).Methods("GET")
	httpServer := httptest.NewServer(mux)

	req, err := http.Get(httpServer.URL + "/ping")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)
}
