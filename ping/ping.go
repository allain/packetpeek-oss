package ping

import (
	"fmt"
	"net/http"
)

// Ping is just an endpoint for ping/pong
func Ping(w http.ResponseWriter, r *http.Request) {
	respJSON := []byte(fmt.Sprintf(`{ "message": "pong!" }`))
	w.WriteHeader(http.StatusOK)
	w.Write(respJSON)
}
