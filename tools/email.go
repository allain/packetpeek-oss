package tools

import (
	"errors"
	"fmt"
	"net/smtp"
)

// Credit: https://gist.github.com/andelf/5118732

type Credentials struct {
	Username string
	Password string
}

func LoginAuth(username, password string) smtp.Auth {
	return &Credentials{username, password}
}

func (c *Credentials) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

func (c *Credentials) Next(challenge []byte, more bool) ([]byte, error) {
	if more {
		switch string(challenge) {
		case "Username:":
			return []byte(c.Username), nil
		case "Password:":
			return []byte(c.Password), nil
		default:
			return nil, errors.New("unkown challenge")
		}
	}
	return nil, nil
}

type SMTP struct {
	User string
	Pass string
	Host string
	Port int
}

type Alert struct {
	Type    string
	To      []string
	Message []byte
	Auth    smtp.Auth
	SMTP    SMTP
}

func SendAlert(alert Alert) error {
	if alert.SMTP.User == "" || alert.SMTP.Pass == "" {
		return fmt.Errorf("No SMTP credentials, cant send " + alert.Type + " alert!")
	}
	return smtp.SendMail(fmt.Sprintf("%s:%d", alert.SMTP.Host, alert.SMTP.Port), alert.Auth, alert.SMTP.User, alert.To, alert.Message)
}
