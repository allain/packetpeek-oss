from urllib.parse import urlencode
from urllib.request import Request, urlopen
import random


def d():
    options = ['p', 'd', 'q', 'e']
    return options[random.randint(0, 3)]


default_bat_lev = 3107  # ten more than 100% due to loop method
url = 'http://localhost:8081/datum'
for x in range(75):
    default_bat_lev -= random.randint(0, 7)
    post_fields = {
        'id': 'a1:b2:c3:d4:e5:f6',
        'bat_lev': default_bat_lev,
        'temp': random.randint(2731, 3031),
        'hum': random.randint(195, 4096),
        'door': d(),
        'nick': 'Demo Device 1',
        'ph': (random.uniform(0.0, 14.0))
    }
    request = Request(url, urlencode(post_fields).encode())
    json = urlopen(request).read().decode()
    print(post_fields)

second_bar_lev = 3107
for x in range(75):
    second_bar_lev -= random.randint(0, 9)
    post_fields = {
        'id': 'f6:e5:d4:c3:b2:a1',
        'bat_lev': second_bar_lev,
        'temp': random.randint(2731, 3031),
        'hum': random.randint(195, 4096),
        'door': d(),
        'nick': 'Demo Device 2',
        'ph': round(random.uniform(0.0, 14.0))
    }
    request = Request(url, urlencode(post_fields).encode())
    json = urlopen(request).read().decode()

print("Done!")
