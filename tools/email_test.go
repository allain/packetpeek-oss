package tools

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoginAuthReturnsCredentials(t *testing.T) {
	assert := assert.New(t)

	assert.Equal(&Credentials{Username: "username", Password: "password"}, LoginAuth("username", "password"))
}

func TestNext(t *testing.T) {
	assert := assert.New(t)

	c := LoginAuth("username", "password")

	nU, err := c.Next([]byte("Username:"), true)
	assert.NoError(err)
	assert.Equal([]byte("username"), nU)

	nP, err := c.Next([]byte("Password:"), true)
	assert.NoError(err)
	assert.Equal([]byte("password"), nP)

	n, err := c.Next([]byte("unknown:"), true)
	assert.Error(err)
	assert.Nil(n)

	n, err = c.Next(nil, false)
	assert.NoError(err)
	assert.Nil(n)
}

func TestSendAlert(t *testing.T) {
	assert := assert.New(t)

	assert.Error(SendAlert(Alert{}))
	smtp := SMTP{
		User: "test@example.com",
		Pass: "password",
		Host: "smtp.example.com",
		Port: 587,
	}
	al := Alert{
		SMTP: smtp,
	}
	assert.Error(SendAlert(al))
}
