package tools

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUpdater(t *testing.T) {
	assert := assert.New(t)

	v, e := CheckForUpdates()
	assert.Equal(false, v)
	assert.Error(e)
}
