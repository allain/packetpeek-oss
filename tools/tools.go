package tools

import (
	"fmt"
	"math"
	"strconv"

	//"net/http"

	"math/rand"
	//log "github.com/sirupsen/logrus"
)

//=======================
//=		Structural		=
//=======================

// SendMail sends an email to to provided recipient
// func sendMail(to, subject, msg string) {
// 	//Create and configure mailer
// 	m := gomail.NewMessage()
// 	m.SetAddressHeader("From", "support@packetpeek.com", "Packetpeek Support")
// 	m.SetHeader("To", to)
// 	m.SetHeader("Subject", subject)
// 	m.SetBody("text/plain", msg)
// 	//Prepare email values
// 	d := gomail.NewPlainDialer("", 587, "", "")
// 	//Send email with error handling
// 	if err := d.DialAndSend(m); err != nil {
// 		panic(err)
// 	}
// }

// PadString will pad a string with 'pad' characters up to 'length' length
func PadString(str, pad string, length int) string {
	for {
		str += pad
		if len(str) > length {
			return str[0:length]
		}
	}
}

// PadNum will prepend a number with 0 for clean formatting where needed
func PadNum(value int) string {
	return fmt.Sprintf("%02d", value)
}

//=======================
//=		Mathematics		=
//=======================

// RandSeq generates a random string of n length
func RandSeq(n int) string {
	letters := []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// MakeNum converts a string to an integer
func MakeNum(in string) int {
	num, err := strconv.Atoi(in)
	if err != nil {
		return 0
	}
	return num
}

func MakeFloat(in string, round float64) float64 {
	num, err := strconv.ParseFloat(in, 64)
	if err != nil {
		return 0.0
	}
	return RoundFloat(num, round)
}

func RoundFloat(val, round float64) float64 {
	return math.Round(val/round) * round
}

func TrimFloatPointOne(val float64) string {
	return fmt.Sprintf("%.1f", val)
}

// Battery2percent converts a millivolts value into a rough capacity percentage value
func Battery2percent(b int) float64 {
	battPercent := 0.0
	if float64(b) >= 3100 {
		battPercent = 100
		return battPercent
	}
	if float64(b) > 2600 {
		battPercent = (float64(b) - 2650) * 100 / (3100 - 2600)
		return battPercent
	} else if float64(b) <= 2600 {
		battPercent = 0
	}
	return battPercent
}

// Dk2cf converts DeciKelvin values to celsius, fahrenheit
func Dk2cf(t int) (float64, float64) {
	var temperatureC, temperatureF float64
	temperatureC = math.Round(((float64(t)/10)-273.15)*100) / 100
	temperatureF = math.Round((((float64(t)/10)*1.8)-459.67)*100) / 100

	return temperatureC, temperatureF
}

// Resistance2moisture takes the resistance value and converts it to a moisture percentage
func Resistance2moisture(m int) float64 {
	var moisture float64
	//moisture = 100 - (float64(m)/4096)*100
	moisture = 100 * (4096 - float64(m)) / (20 * float64(m+1)) // TODO: this seems off
	return math.Round(moisture*100) / 100
}

// TranslateDoorStatus takes the status character and translates it to English
func TranslateDoorStatus(d string) string {
	var status string
	switch d {
	case "q", "r": // r = door wide open & flood detected
		status = "Wide Open"
	case "e", "f": // f = door closed shut & flood detected
		status = "Closed Shut"
	case "p":
		status = "Recently Opened"
	case "d":
		status = "Recently Closed"
	default:
		status = "Unknown"
	}

	return status
}

//=======================
//=		Cryptography	=
//=======================

// HashPassword will hash a provided password
// func HashPassword(password string) (string, error) {
// 	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
// 	return string(bytes), err
// }

// CheckPasswordHash compares a provided password to a stored hash
// func CheckPasswordHash(password, hash string) bool {
// 	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
// 	return err == nil //Wouldn't this hard code a nill error?
// }
