package biot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// CREATE

func TestAttemptAutoRegistration(t *testing.T) {
	assert := assert.New(t)

	d := makeDevicePacket()
	var dev Device
	assert.NoError(json.Unmarshal(d, &dev))
	assert.NoError(GetH().AttemptAutoRegistration(dev))
	// Fail on duplicate, dont register existing devices even though we should never reach that point
	assert.Error(GetH().AttemptAutoRegistration(dev))

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestAttemptAutoRegistrationSetsCorrectDefaults(t *testing.T) {
	assert := assert.New(t)

	dev := Device{
		DeviceID: "1a:2b:3c:4d:5e:6f",
		Nickname: "test",
	}
	assert.NoError(GetH().AttemptAutoRegistration(dev))

	httpServer := httptest.NewServer(GetH().Router())
	req, err := http.Get(httpServer.URL + "/device/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("default", resp["melody"])
	assert.Equal("p", resp["dooralert"])
	// Decoding throws all numbers into floats to be safe+lazy
	assert.Equal(float64(6), resp["updateHours"])
	assert.Equal(float64(0), resp["updateMinutes"])
	assert.Equal(float64(15), resp["alertMinutes"])
	assert.Equal(float64(1), resp["mode"])

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDeviceCreate(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDeviceCreateFailsOnInsertError(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()
	registerTestDevice(httpServer, t)

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusConflict, req.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDeviceCreateDefaultsUpdateAndAlertTiming(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := []byte(fmt.Sprintf(`{"id":"1a:2b:3c:4d:5e:6f","nickname":"testdevice"}`))

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/device/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// READ

func TestDeviceRead(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/device/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("1a:2b:3c:4d:5e:6f", resp["id"])

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDeviceReadBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/device/bad_id")
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, req.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// READ ALL

func TestDeviceReadAll(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/devices")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	var resp []map[string]interface{}
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("1a:2b:3c:4d:5e:6f", resp[0]["id"])

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// UPDATE

func TestDeviceUpdate(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	device2 := map[string]interface{}{
		"DeviceID":      "99:99:99:99:99:99",
		"Nickname":      "newname",
		"UpdateHours":   99,
		"UpdateMinutes": 99,
		"AlertMinutes":  99,
	}

	data2Json, err := json.Marshal(device2)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/device/1a:2b:3c:4d:5e:6f", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDeviceUpdateBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	device22 := Device{
		DeviceID:      "99:99:99:99:99:99",
		Nickname:      "newname",
		UpdateHours:   99,
		UpdateMinutes: 99,
		AlertMinutes:  99,
	}

	data2Json, err := json.Marshal(device22)
	assert.NoError(err)

	req3, err := http.NewRequest("PATCH", httpServer.URL+"/device/9999", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req3)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDeviceUpdateBadPayloadReturnsNil(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	badBytes := []byte(`badbytes`)
	req4, err := http.NewRequest("PATCH", httpServer.URL+"/device/1a:2b:3c:4d:5e:6f", bytes.NewBuffer(badBytes))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req4)
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// DELETE

func TestDeviceDelete(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/device/1a:2b:3c:4d:5e:6f", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNoContent, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDeviceDeleteBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/device/9999", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// HELPER FUNCTIONS

func makeDevicePacket() []byte {
	deviceData := []byte(fmt.Sprintf(`{"id":"1a:2b:3c:4d:5e:6f","nickname":"testdevice","updateHours":6,"updateMinutes":0,"alertMinutes":15, "melody":"default"}`))

	return deviceData
}
