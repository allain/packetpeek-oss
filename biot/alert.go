package biot

type Alert struct {
	Email  string
	Reason string
}
