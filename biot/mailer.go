package biot

import (
	"fmt"

	"gitlab.com/jonjonnetworks/packetpeek-oss/tools"
)

func AssembleAlert(email, reason string, d Device, s Settings) (tools.Alert, error) {
	if s.SMTPUser == "" || s.SMTPPass == "" {
		return tools.Alert{}, fmt.Errorf("No SMTP credentials, cant send " + reason + " alert!")
	}

	msgContent := "To: " + email + "\r\n" + "Subject: " + d.Nickname
	switch reason {
	case "flood":
		msgContent += floodtemplate
	case "door":
		msgContent += doortemplate
	case "water":
		msgContent += watertemplate
	}

	al := tools.Alert{
		Type:    reason,
		To:      []string{email},
		Message: []byte(msgContent),
		Auth:    tools.LoginAuth(s.SMTPUser, s.SMTPPass),
		SMTP: tools.SMTP{
			User: s.SMTPUser,
			Pass: s.SMTPPass,
			Host: s.SMTPHost,
			Port: s.SMTPPort,
		},
	}

	return al, nil
}

var floodtemplate = "Flood Alert!\r\n\r\nBIOT reported a flood alert!\r\n"
var watertemplate = "Just Got Watered!\r\n\r\nBIOT reported being watered!\r\n"
var doortemplate = "Door Alert!\r\n\r\nBIOT reported a door alert!\r\n"
