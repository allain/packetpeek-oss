package biot

import (
	"fmt"
	"net/http"
	"os"

	"github.com/alexbyk/panicif"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/jonjonnetworks/packetpeek-oss/ping"
)

// Router creates our Mux routing
func (h *Handler) Router() *mux.Router {
	mux := mux.NewRouter().StrictSlash(true)

	// For programmatic polling
	mux.HandleFunc("/ping", ping.Ping).Methods("GET")

	mux.HandleFunc("/settings", h.SettingsRead).Methods("GET")
	mux.HandleFunc("/settings", h.SettingUpdate).Methods("PATCH")

	mux.HandleFunc("/data", h.DataReadAll).Methods("GET")
	mux.HandleFunc("/data/{id}", h.DataRead).Methods("GET")

	mux.HandleFunc("/devices", h.DeviceReadAll).Methods("GET")
	mux.HandleFunc("/device/{id}", h.DeviceRead).Methods("GET")

	mux.HandleFunc("/home", h.Home).Methods("GET")
	mux.HandleFunc("/home/{id}", h.Home).Methods("GET")

	fs := http.FileServer(http.Dir("./frontend"))
	mux.PathPrefix("/assets/").Handler(fs)
	mux.PathPrefix("/css/").Handler(fs)
	mux.Handle("/", fs)

	// Dont include Create, Update, or Delete routes
	if os.Getenv("PPKENV") == "READONLY" {
		log.Warn("*** Server in ReadOnly mode! ***")
		return mux
	}

	mux.HandleFunc("/datum", func(w http.ResponseWriter, r *http.Request) {
		// POST request does not come from a BIOT
		statusCode, responseBody := h.DataCreate(w, r, false)
		w.WriteHeader(statusCode)
		w.Write(responseBody)
	}).Methods("POST")
	mux.HandleFunc("/data/{id}", h.DataUpdate).Methods("PATCH")
	mux.HandleFunc("/data/{id}", h.DataDelete).Methods("DELETE")

	mux.HandleFunc("/device", h.DeviceCreate).Methods("POST")
	mux.HandleFunc("/device/{id}", h.DeviceUpdate).Methods("PATCH")
	mux.HandleFunc("/device/{id}", h.DeviceDelete).Methods("DELETE")

	return mux
}

// BIOTListener creates another webserver just for the BIOT to post data to
func MakeBIOTListener(h *Handler, port int) (string, *mux.Router) {
	listener := mux.NewRouter()
	listener.HandleFunc("/dev/f1", h.Post).Methods("POST").Name("biot")
	return fmt.Sprintf(":%d", port), listener
}

// CreateHandler builds a new Handler containing device and data handling
func CreateHandler(devicesdb, datadb, settingsdb string) *Handler {
	biotDevice, biotData, settings, err := CreateDatabases(devicesdb, datadb, settingsdb)
	panicif.Err(err)
	return &Handler{Device: biotDevice, Data: biotData, Settings: settings}
}
