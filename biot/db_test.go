package biot

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateBiotDatabases(t *testing.T) {
	assert := assert.New(t)

	testDevice, testData, testSettings, err := CreateDatabases(testDevicesTable, testDataTable, testSettingsTable)
	assert.NoError(err)
	h := &Handler{Device: testDevice, Data: testData, Settings: testSettings}
	assert.NotNil(h)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestCreateBiotDatabasesErrorsOnBadDBNames(t *testing.T) {
	assert := assert.New(t)

	junkDevice, junkData, junkSettings, err := CreateDatabases("notadb", "thiseither", "nope")
	assert.Error(err)
	assert.Nil(junkDevice)
	assert.Nil(junkData)
	assert.Nil(junkSettings)
}

func TestCreateBiotDatabasesErrorsOnEmptyDBNames(t *testing.T) {
	assert := assert.New(t)

	noDevice, noData, noSettings, err := CreateDatabases("", "", "")
	assert.Error(err)
	assert.Nil(noDevice)
	assert.Nil(noData)
	assert.Nil(noSettings)
}
