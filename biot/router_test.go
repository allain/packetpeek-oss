package biot

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

const testDevicesTable = "test_devices.db"
const testDataTable = "test_data.db"
const testSettingsTable = "test_settings.db"

// ROUTER

func TestRouter(t *testing.T) {
	assert := assert.New(t)

	assert.NotNil(GetH().Router())

	err := DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable)
	assert.NoError(err)
}

func DeleteTestDatabases(databases ...string) error {
	for _, d := range databases {
		if err := os.Remove(d); err != nil {
			return err
		}
	}
	return nil
}

func TestMakeBiotHandler(t *testing.T) {
	assert := assert.New(t)
	p, r := MakeBIOTListener(GetH(), 7875)

	assert.Equal(":7875", p)
	assert.NotNil(r.Get("biot"))
}

// Helper function for tests to generate handlers neatly
func GetH() *Handler {
	return CreateHandler(testDevicesTable, testDataTable, testSettingsTable)
}
