package biot

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// CREATE

func TestDataCreateFromClient(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// READ

func TestDataRead(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/data/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("1a:2b:3c:4d:5e:6f", resp["deviceID"])

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDataReadBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/data/99:99:99:99:99:99")
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, req.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// READ ALL

func TestDataReadAll(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/data")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	var resp []map[string]interface{}
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("1a:2b:3c:4d:5e:6f", resp[0]["deviceID"])

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// UPDATE

func TestDataUpdate(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	data2 := map[string]interface{}{
		"DeviceID":    "99:99:99:99:99:99",
		"Battery":     1000,
		"Temperature": 1000,
		"Moisture":    100,
		"HallSense":   "d",
		"Nickname":    "UPDATED",
	}

	data2Json, err := json.Marshal(data2)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/data/1", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDataUpdateBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	data2 := map[string]interface{}{
		"DeviceID":    "99:99:99:99:99:99",
		"Battery":     1000,
		"Temperature": 1000,
		"Moisture":    100,
		"HallSense":   "d",
		"Nickname":    "UPDATED",
	}

	data2Json, err := json.Marshal(data2)
	assert.NoError(err)

	req3, err := http.NewRequest("PATCH", httpServer.URL+"/data/99:99:99:99:99:99", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req3)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDataUpdateBadPayloadReturnsNil(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	badBytes := []byte(`badbytes`)
	req4, err := http.NewRequest("PATCH", httpServer.URL+"/data/1", bytes.NewBuffer(badBytes))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req4)
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

// DELETE

func TestDataDelete(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/data/1", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNoContent, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestDataDeleteBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/data/99:99:99:99:99:99", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestGenerateBiotResponse(t *testing.T) {
	assert := assert.New(t)
	h := GetH()
	httpServer := httptest.NewServer(h.Router())

	device := makeDevicePacket()
	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	data := Data{DeviceID: "1a:2b:3c:4d:5e:6f"}
	song := h.generateBiotResponse(data)
	assert.Equal(uint8(0xCC), song[2])
	assert.Equal(uint8(0x9A), song[64])
	assert.Equal(uint8(0x5E), song[65])
	assert.Equal(uint8(0x3A), song[66])
	assert.Equal(uint8(0x06), song[3])
	assert.Equal(uint8(0x00), song[4])
	assert.Equal(uint8(0x0f), song[5])

	// Ensure we get error tones on orphaned data
	data2 := Data{DeviceID: "ff:ff:ff:ff:ff:ff"}
	song2 := h.generateBiotResponse(data2)
	assert.Equal(uint8(0xCC), song2[2])
	assert.Equal(uint8(0x3A), song2[64])
	assert.Equal(uint8(0x5E), song2[65])
	assert.Equal(uint8(0x9A), song2[66])

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestCheckAlert(t *testing.T) {
	assert := assert.New(t)

	assert.Error(checkAlert(Device{}, Data{DoorStatus: "r"}, Settings{}))
	assert.Error(checkAlert(Device{Mode: 2}, Data{DoorStatus: "r"}, Settings{}))
	assert.Error(checkAlert(Device{DoorAlert: "p"}, Data{DoorStatus: "p"}, Settings{}))

	// Take it all the way to actually sending email, but will fail on SMTP login
	s := Settings{
		SMTPUser: "test@example.com",
		SMTPPass: "password",
		SMTPHost: "smtp.example.com",
		SMTPPort: 587,
	}
	assert.Error(checkAlert(Device{Nickname: "test"}, Data{DoorStatus: "f"}, s))
}

func TestGetBiotSong(t *testing.T) {
	assert := assert.New(t)
	assert.Equal("\xFF", getBiotSong("silent"))
	assert.Equal("\x9A\x5E\x3A", getBiotSong("default"))
	assert.Equal("\x3A\x5E\x9A", getBiotSong("error"))
	assert.Equal("\x3A\x5E\x3A\x5E\x3A\x5E\x3A\x5E", getBiotSong("alert"))
	assert.Equal("\x3A\x3A\x3A", getBiotSong("undefined"))
}

// HELPER FUNCTIONS

func registerTestDevice(httpServer *httptest.Server, t *testing.T) {
	assert := assert.New(t)

	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)
}

func makeDataPacket() url.Values {
	data := url.Values{}
	data.Set("id", "1a:2b:3c:4d:5e:6f")
	data.Set("bat_lev", "2905")
	data.Set("temp", "2995")
	data.Set("hum", "240")
	data.Set("door", "q")
	data.Set("nick", "testdevice")

	return data
}
