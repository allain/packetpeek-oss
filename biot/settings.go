package biot

import (
	"encoding/json"
	"fmt"

	// "net"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"

	// "github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// Settings holds our dashboard settings
type Settings struct {
	gorm.Model
	Name        string
	Email       string
	Timezone    string
	Units       string
	Port        int
	BIOTPort    int
	LimitPorts  bool
	SMTPUser    string
	SMTPPass    string
	SMTPHost    string
	SMTPPort    int
	Alerts      bool
	GraphPoints int
}

func (h *Handler) SettingsInit() error {
	// Make sure there are no existing settings
	var s Settings
	h.Settings.Find(&s)
	if s.Name != "" && s.Email != "" && s.Timezone != "" && s.Units != "" && s.Port != 0 && s.GraphPoints != 0 {
		return fmt.Errorf("skip")
	}

	var d []byte
	var err error
	d, err = ioutil.ReadFile("default_settings.json")
	if err != nil {
		log.Warn("No default settings file 'default_settings.json' found. Using built-in defaults.")
		d = []byte(`{ "Name":"Demo User", "Email":"none@example.com", "Timezone":"America/Toronto", "Units":"Metric", "Port":8081, "BIOTPort":7875, "LimitPorts":true, "Alerts":false, "GraphPoints":75 }`)
	}

	if err = json.Unmarshal(d, &s); err != nil {
		fmt.Println("error:", err)
	}

	if s.SMTPUser == "" || s.SMTPPass == "" || s.SMTPPort == 0 {
		log.Warn("Missing SMTP credentials, email alerts will be disabled until credentials are added")
	}

	return h.Settings.Create(&s).Error
}

// SettingsRead will return server settings
func (h *Handler) SettingsRead(w http.ResponseWriter, r *http.Request) {
	var settings Settings
	h.Settings.Find(&settings)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(settings)
}

// SettingUpdate will update server settings
func (h *Handler) SettingUpdate(w http.ResponseWriter, r *http.Request) {
	var settingsUpdate map[string]interface{} // Using map so we can update the boolean field to false
	if err := json.NewDecoder(r.Body).Decode(&settingsUpdate); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf(`{ "message": "Error Unmarshaling JSON" }`)))
		return
	}

	var settings Settings
	if h.Settings.First(&settings).Update(settingsUpdate).RowsAffected < 1 {
		respJSON := []byte(fmt.Sprintf(`{ "message": "Settings not found!" }`))
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	w.WriteHeader(http.StatusOK)
}
