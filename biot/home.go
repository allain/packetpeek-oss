package biot

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

type Server struct {
	Version string
	Release string
	ID      string
}

// Home contains everything needed to render the dashboard
type Home struct {
	Server   Server
	Settings Settings
	Devices  []Device
	Data     []Data
}

// Home serves the data to create the dashboard
func (h *Handler) Home(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var home Home
	h.Device.Find(&home.Devices)

	if id == "" {
		if len(home.Devices) == 0 {
			id = "none"
		} else {
			id = home.Devices[0].DeviceID
		}
	}

	version := "1.0.0"
	// branch := "dev"
	server := Server{
		Version: version + "-oss", //+ "-" + branch,
		Release: "June-2021",
		ID:      id,
	}
	home.Server = server

	h.Settings.Find(&home.Settings)

	gp := home.Settings.GraphPoints
	if gp == 0 {
		gp = 75
	}

	h.Data.Limit(gp).Where("device_id = ?", id).Order("created_at desc").Find(&home.Data)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(home)
}
