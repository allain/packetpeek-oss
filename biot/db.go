package biot

import (
	"errors"

	"github.com/alexbyk/panicif"
	"github.com/jinzhu/gorm"
)

// Handler exposes our database handlers
type Handler struct {
	Device 		*gorm.DB
	Data   		*gorm.DB
	Settings	*gorm.DB
}

// CreateBiotDatabases creates and returns the GORM databases
func CreateDatabases(devicesTable, dataTable, settingsTable string) (*gorm.DB, *gorm.DB, *gorm.DB, error) {
	if devicesTable == "" || dataTable == "" || settingsTable == "" {
		return nil, nil, nil, errors.New("Missing table name(s)")
	}

	runeDevices := []rune(devicesTable)
	runeData := []rune(dataTable)
	runeSettings := []rune(settingsTable)

	devicesSubstring := string(runeDevices[len(devicesTable)-3:])
	dataSubstring := string(runeData[len(dataTable)-3:])
	settingsSubstring := string(runeSettings[len(settingsTable)-3:])
	
	if devicesSubstring != ".db" || dataSubstring != ".db" || settingsSubstring != ".db" {
		return nil, nil, nil, errors.New("Incorrect db name supplied. Must end in '.db'")
	}

	devices, err := gorm.Open("sqlite3", devicesTable)
	panicif.Err(err)

	data, err := gorm.Open("sqlite3", dataTable)
	panicif.Err(err)

	settings, err := gorm.Open("sqlite3", settingsTable)
	panicif.Err(err)

	devices.AutoMigrate(&Device{})
	data.AutoMigrate(&Data{})
	settings.AutoMigrate(&Settings{})
	return devices, data, settings, nil
}