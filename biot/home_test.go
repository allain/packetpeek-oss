package biot

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHome(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())

	// Test before devices exist to get ID of "none"
	noneReq, err := http.Get(httpServer.URL + "/home")
	assert.NoError(err)
	assert.Equal(http.StatusOK, noneReq.StatusCode)
	var none Home
	noneDecoder := json.NewDecoder(noneReq.Body)
	err = noneDecoder.Decode(&none)
	assert.NoError(err)
	assert.Equal("none", none.Server.ID)

	registerTestDevice(httpServer, t)
	data := makeDataPacket()
	// device := makeDevicePacket()

	dataReq, err := http.Post(httpServer.URL+"/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, dataReq.StatusCode)

	homeReq, err := http.Get(httpServer.URL + "/home")
	assert.NoError(err)
	assert.Equal(http.StatusOK, homeReq.StatusCode)

	var resp Home
	decoder := json.NewDecoder(homeReq.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)

	assert.Equal("1a:2b:3c:4d:5e:6f", resp.Devices[0].DeviceID)
	assert.Equal("testdevice", resp.Data[0].Nickname)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}
