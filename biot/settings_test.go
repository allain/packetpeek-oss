package biot

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSettingsInit(t *testing.T) {
	assert := assert.New(t)

	h := GetH()
	assert.NoError(h.SettingsInit())

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestSettingsInitSkipsIfExist(t *testing.T) {
	assert := assert.New(t)

	h := GetH()
	assert.NoError(h.SettingsInit())
	assert.Error(h.SettingsInit())

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestSettingRead(t *testing.T) {
	assert := assert.New(t)

	h := GetH()

	httpServer := httptest.NewServer(h.Router())
	h.SettingsInit()

	req, err := http.Get(httpServer.URL + "/settings")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal(true, resp["LimitPorts"])

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestSettingUpdate(t *testing.T) {
	assert := assert.New(t)

	h := GetH()

	httpServer := httptest.NewServer(h.Router())
	h.SettingsInit()

	set := map[string]interface{}{"LimitPorts": false}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	req, err := http.Get(httpServer.URL + "/settings")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	response := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&response)
	assert.NoError(err)
	assert.Equal(false, response["LimitPorts"])

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestSettingUpdateFailsOnNoRowsAffected(t *testing.T) {
	assert := assert.New(t)

	h := GetH()

	httpServer := httptest.NewServer(h.Router())
	h.SettingsInit()

	set := map[string]interface{}{"oranges": false}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}

func TestSettingUpdateFailsOnBadInput(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(GetH().Router())

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, resp.StatusCode)

	assert.NoError(DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}
