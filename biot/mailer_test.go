package biot

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jonjonnetworks/packetpeek-oss/tools"
)

func TestAssembleAlertErrorsOnMissingInfo(t *testing.T) {
	assert := assert.New(t)

	a, e := AssembleAlert("", "", Device{}, Settings{})
	assert.Error(e)
	assert.Equal(tools.Alert{}, a)
}

func TestAssembleAlert(t *testing.T) {
	assert := assert.New(t)

	email := "test@example.com"
	d := Device{Nickname: "test"}
	s := Settings{
		SMTPUser: "test@example.com",
		SMTPPass: "password",
		SMTPHost: "smtp.example.com",
		SMTPPort: 587,
	}

	a, e := AssembleAlert(email, "flood", d, s)
	assert.NoError(e)
	assert.NotNil(a.Auth)
	assert.Equal([]string([]string{email}), a.To)
	assert.Equal("flood", a.Type)
	assert.Equal(s.SMTPUser, a.SMTP.User)
	assert.Equal(s.SMTPPass, a.SMTP.Pass)
	assert.Equal(s.SMTPHost, a.SMTP.Host)
	assert.Equal(s.SMTPPort, a.SMTP.Port)
	msg := "To: " + email + "\r\n" +
		"Subject: " + d.Nickname + "Flood Alert!\r\n\r\nBIOT reported a flood alert!\r\n"
	assert.Equal([]byte(msg), a.Message)

	a, _ = AssembleAlert(email, "door", d, s)
	assert.Equal("door", a.Type)
	a, _ = AssembleAlert(email, "water", d, s)
	assert.Equal("water", a.Type)
}
