# Packetpeek OSS Edition

Welcome to Packetpeek Open Source Software edition!

[![Coverage Status](https://coveralls.io/repos/gitlab/jonjonnetworks/packetpeek-oss/badge.svg?branch=dev)](https://coveralls.io/gitlab/jonjonnetworks/packetpeek-oss?branch=dev)

Proper description & setup instructions to come.

Check the [Demo](https://packetpeek.jonahstfrancois.com)
