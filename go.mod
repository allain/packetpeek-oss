module gitlab.com/jonjonnetworks/packetpeek-oss

go 1.13

require (
	cloud.google.com/go v0.37.4 // indirect
	github.com/alexbyk/panicif v1.1.0
	github.com/axw/gocov v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	golang.org/x/sys v0.0.0-20210423185535-09eb48e85fd7 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
