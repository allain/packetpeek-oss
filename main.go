package main

import (
	"fmt"
	"net/http"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/jonjonnetworks/packetpeek-oss/biot"
)

func init() {
	log.SetFormatter(&log.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		FullTimestamp:   true,
	})
	log.Warn("The SQlite3 errors above are normal, and come from upstream.")
	log.Info("Initiating server...")
}

func main() {
	// go udp.UDPDiscovery()

	h := biot.CreateHandler("biot_devices.db", "biot_data.db", "settings.db")
	if err := h.SettingsInit(); err != nil {
		if err.Error() != "skip" {
			panic(err.Error())
		}
		log.Info("Existing Settings Found, skipping default settings import.")
	}

	var settings biot.Settings
	h.Settings.First(&settings)

	// Separate handler for incoming BIOT data
	log.Info(fmt.Sprintf("Starting BIOT listener on port %d...\n", settings.BIOTPort))
	go http.ListenAndServe(biot.MakeBIOTListener(h, settings.BIOTPort))

	log.Infof("Starting webserver on port %d...\n", settings.Port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", settings.Port), cors.Default().Handler(h.Router()))
	if err != nil {
		panic(err)
	}
}
